﻿DROP DATABASE IF EXISTS h8ejercicio2;
CREATE DATABASE h8ejercicio2;
USE h8ejercicio2;

CREATE OR REPLACE table persona(
  dni varchar(9),
  nombrePers varchar(20),
  edad int,
  PRIMARY KEY (dni)
  );


CREATE OR REPLACE TABLE piso(
  planta int,
  puerta varchar(10),
  calle varchar(20),
  numero int,
  m2 float,
  PRIMARY KEY (planta, puerta, calle, numero)
  );




CREATE OR REPLACE TABLE viveP(
  dniPersona varchar(9),
  planta int,
  puerta varchar(10),
  calle varchar(20),
  numero int,
  PRIMARY KEY (dniPersona, planta, puerta, calle, numero),
  UNIQUE (dniPersona),
  CONSTRAINT fkviveP_persona FOREIGN KEY (dniPersona) REFERENCES persona(dni),
  CONSTRAINT fkviveP_planta FOREIGN KEY (planta, puerta, calle, numero) REFERENCES piso(planta, puerta, calle, numero)
  );



CREATE OR REPLACE TABLE poseeP(
  dniPersona varchar(9),
  planta int, 
  puerta varchar(10),
  calle varchar(20),
  numero int,
  PRIMARY KEY(dniPersona, planta, puerta, calle, numero),
  CONSTRAINT fkposeeP_persona FOREIGN KEY (dniPersona) REFERENCES persona (dni),
  CONSTRAINT fkposeeP_piso FOREIGN KEY (planta, puerta, calle, numero) REFERENCES piso (planta, puerta, calle, numero)
  );

CREATE OR REPLACE TABLE zona_urbana(
  nombreZona varchar(20),
  categoria  varchar(20),
  PRIMARY KEY (nombreZona)
  );

CREATE OR REPLACE TABLE bloque_casas(
  calle varchar(20),
  numero int,
  nPisos int,
  PRIMARY KEY (calle, numero)
  );

CREATE OR REPLACE TABLE z_bloque(
  nombreZona varchar(20),
  calle varchar(20),
  numero int,
  PRIMARY KEY (nombreZona, calle, numero), 
  CONSTRAINT fkzonaBloque_zona FOREIGN KEY (nombreZona) REFERENCES zona_urbana (nombreZona),
  CONSTRAINT fkzonaBloque_bloque FOREIGN KEY (calle, numero) REFERENCES bloque_casas (calle, numero)
  );

CREATE OR REPLACE TABLE casa_particular(
  nCasa int,
  nombreZona varchar(20),
  m2 float,
  PRIMARY KEY (nCasa, nombreZona),
  CONSTRAINT fkcasaParticular_Zona FOREIGN KEY (nombreZona) REFERENCES zona_urbana (nombreZona)
  );

CREATE OR REPLACE TABLE viveC(
  dniPersona varchar(9),
  nCasa int, 
  nombreZona varchar(20),
  PRIMARY KEY (dniPersona, nCasa, nombreZona),
  UNIQUE KEY (dniPersona),
  CONSTRAINT fkviveC_persona FOREIGN KEY (dniPersona) REFERENCES persona (dni),
  CONSTRAINT fkviveC_casa FOREIGN KEY (nCasa, nombreZona) REFERENCES casa_particular (nCasa, nombreZona)
  );


CREATE OR REPLACE TABLE poseeC(
  dniPersona varchar(9),
  nCasa int,
  nombreZona varchar(20),
  PRIMARY KEY (dniPersona, nCasa, nombreZona),
  CONSTRAINT fkposeeC_persona FOREIGN KEY (dniPersona) REFERENCES persona (dni),
  CONSTRAINT fkposeec_casa FOREIGN KEY (nCasa,nombreZona) REFERENCES casa_particular (nCasa, nombreZona)
  );
